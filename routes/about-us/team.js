var express = require('express')
var router = express.Router()
var User = require('../../src/entity/User')

router.get('/', function (req, res) {
    const teamMembers = []

    teamMembers.push(
        new User('Karla', 'Vogt', 25, 'Graphic Designer', '/images/persons/girl1.jpg'),
        new User('Valentin', 'Weiß', 43, 'Project Manager', '/images/persons/man1.jpg'),
        new User('Karoline', 'Dreher', 39, 'Director', '/images/persons/girl2.jpg'),
        new User('Leon', 'Scholz', 43, 'Project Manager', '/images/persons/man2.jpg')
    )

    res.render('about-us/team', {
        title: 'Team',
        teamMembers: teamMembers
    })
})

module.exports = router