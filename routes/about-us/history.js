var express = require('express')
var router = express.Router()

router.get('/', function (req, res) {
    res.render('about-us/history', {
        title: 'History',
        startYear: 2000,
        toCorporateYear: 2014,
        currentYear: new Date().getFullYear()
    })
})

module.exports = router