"use strict";
exports.__esModule = true;
var express = require('express');
var router = express.Router();
var node_fetch_1 = require("node-fetch"); // node-fetch must be 2.6.1 and below
var dataVerkehrstote;
(0, node_fetch_1["default"])('http://kfv-chart.2doworx.com/json.json')
    .then(function (response) {
    return response.json();
})
    .then(function (myJson) {
    dataVerkehrstote = myJson;
    // console.log(typeof dataVerkehrstote.verkehrstote[0]) // Object
    // console.log(dataVerkehrstote.verkehrstote.length) // 13127
    // console.log(dataVerkehrstote.verkehrstote[0].Berichtsjahr) // 2011
    var berichtsjahrArr = [];
    var berichtsJahreUnique;
    var getoeteteWien = [];
    var getoeteteVor = [];
    var getoeteteTirol = [];
    var getoeteteSteir = [];
    var getoeteteSalz = [];
    var getoeteteOber = [];
    var getoeteteNieder = [];
    var getoeteteKaertner = [];
    var getoeteteBurg = [];
    Object.entries(dataVerkehrstote.verkehrstote).forEach(function (_a) {
        var index = _a[0], singleData = _a[1];
        berichtsjahrArr.push(Number(singleData.Berichtsjahr));
    });
    berichtsJahreUnique = Array.from(new Set(berichtsjahrArr)).sort();
    Object.entries(berichtsJahreUnique).forEach(function (_a) {
        var index = _a[0], year = _a[1];
        var numberOfDeadWien = 0;
        var numberOfDeadVor = 0;
        var numberOfDeadTirol = 0;
        var numberOfDeadSteir = 0;
        var numberOfDeadSalz = 0;
        var numberOfDeadOber = 0;
        var numberOfDeadNieder = 0;
        var numberOfDeadKaertner = 0;
        var numberOfDeadBurg = 0;
        Object.entries(dataVerkehrstote.verkehrstote).forEach(function (_a) {
            var index = _a[0], singleData = _a[1];
            if (year === Number(singleData.Berichtsjahr)) {
                if (Number(singleData.Bundesland_ID) === 3) {
                    numberOfDeadWien += Number(singleData.Getötete);
                }
                if (Number(singleData.Bundesland_ID) === 4) {
                    numberOfDeadVor += Number(singleData.Getötete);
                }
                if (Number(singleData.Bundesland_ID) === 5) {
                    numberOfDeadTirol += Number(singleData.Getötete);
                }
                if (Number(singleData.Bundesland_ID) === 6) {
                    numberOfDeadSteir += Number(singleData.Getötete);
                }
                if (Number(singleData.Bundesland_ID) === 7) {
                    numberOfDeadSalz += Number(singleData.Getötete);
                }
                if (Number(singleData.Bundesland_ID) === 2) {
                    numberOfDeadOber += Number(singleData.Getötete);
                }
                if (Number(singleData.Bundesland_ID) === 1) {
                    numberOfDeadNieder += Number(singleData.Getötete);
                }
                if (Number(singleData.Bundesland_ID) === 8) {
                    numberOfDeadKaertner += Number(singleData.Getötete);
                }
                if (Number(singleData.Bundesland_ID) === 8) {
                    numberOfDeadBurg += Number(singleData.Getötete);
                }
            }
        });
        getoeteteWien.push(numberOfDeadWien);
        getoeteteVor.push(numberOfDeadVor);
        getoeteteTirol.push(numberOfDeadTirol);
        getoeteteSteir.push(numberOfDeadSteir);
        getoeteteSalz.push(numberOfDeadSalz);
        getoeteteOber.push(numberOfDeadOber);
        getoeteteNieder.push(numberOfDeadNieder);
        getoeteteKaertner.push(numberOfDeadKaertner);
        getoeteteBurg.push(numberOfDeadBurg);
    });
    var labels = berichtsJahreUnique;
    router.get('/', function (req, res) {
        res.render('statistics/graph', {
            title: 'Graph',
            labels: labels,
            getoeteteWien: getoeteteWien,
            getoeteteVor: getoeteteVor,
            getoeteteTirol: getoeteteTirol,
            getoeteteSteir: getoeteteSteir,
            getoeteteSalz: getoeteteSalz,
            getoeteteOber: getoeteteOber,
            getoeteteNieder: getoeteteNieder,
            getoeteteKaertner: getoeteteKaertner,
            getoeteteBurg: getoeteteBurg
        });
    });
});
module.exports = router;
