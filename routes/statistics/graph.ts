var express = require('express')
var router = express.Router()
import fetch from 'node-fetch'; // node-fetch must be 2.6.1 and below
import {RootJsonObject} from "../../src-ts/interface/RootJsonObject"


let dataVerkehrstote: RootJsonObject

fetch('http://kfv-chart.2doworx.com/json.json')
    .then(function(response) {
        return response.json();
    })
    .then(function(myJson) {
        dataVerkehrstote = myJson

        // console.log(typeof dataVerkehrstote.verkehrstote[0]) // Object
        // console.log(dataVerkehrstote.verkehrstote.length) // 13127
        // console.log(dataVerkehrstote.verkehrstote[0].Berichtsjahr) // 2011

        let berichtsjahrArr: number[] = [];
        let berichtsJahreUnique;
        let getoeteteWien: number[] = [];
        let getoeteteVor: number[] = [];
        let getoeteteTirol: number[] = [];
        let getoeteteSteir: number[] = [];
        let getoeteteSalz: number[] = [];
        let getoeteteOber: number[] = [];
        let getoeteteNieder: number[] = [];
        let getoeteteKaertner: number[] = [];
        let getoeteteBurg: number[] = [];

        Object.entries(dataVerkehrstote.verkehrstote).forEach(([index, singleData]) => {
            berichtsjahrArr.push(Number(singleData.Berichtsjahr));
        });

        berichtsJahreUnique = Array.from(new Set(berichtsjahrArr)).sort();

        Object.entries(berichtsJahreUnique).forEach(([index, year]) => {
            let numberOfDeadWien = 0;
            let numberOfDeadVor = 0;
            let numberOfDeadTirol = 0;
            let numberOfDeadSteir = 0;
            let numberOfDeadSalz = 0;
            let numberOfDeadOber = 0;
            let numberOfDeadNieder = 0;
            let numberOfDeadKaertner = 0;
            let numberOfDeadBurg =  0;

            Object.entries(dataVerkehrstote.verkehrstote).forEach(([index, singleData]) => {
                if (year === Number(singleData.Berichtsjahr)) {
                    if (Number(singleData.Bundesland_ID) === 3) {
                        numberOfDeadWien += Number(singleData.Getötete);
                    }

                    if (Number(singleData.Bundesland_ID) === 4) {
                        numberOfDeadVor += Number(singleData.Getötete);
                    }

                    if (Number(singleData.Bundesland_ID) === 5) {
                        numberOfDeadTirol += Number(singleData.Getötete);
                    }

                    if (Number(singleData.Bundesland_ID) === 6) {
                        numberOfDeadSteir += Number(singleData.Getötete);
                    }

                    if (Number(singleData.Bundesland_ID) === 7) {
                        numberOfDeadSalz += Number(singleData.Getötete);
                    }

                    if (Number(singleData.Bundesland_ID) === 2) {
                        numberOfDeadOber += Number(singleData.Getötete);
                    }

                    if (Number(singleData.Bundesland_ID) === 1) {
                        numberOfDeadNieder += Number(singleData.Getötete);
                    }

                    if (Number(singleData.Bundesland_ID) === 8) {
                        numberOfDeadKaertner += Number(singleData.Getötete);
                    }

                    if (Number(singleData.Bundesland_ID) === 8) {
                        numberOfDeadBurg += Number(singleData.Getötete);
                    }
                }
            });

            getoeteteWien.push(numberOfDeadWien);
            getoeteteVor.push(numberOfDeadVor);
            getoeteteTirol.push(numberOfDeadTirol);
            getoeteteSteir.push(numberOfDeadSteir);
            getoeteteSalz.push(numberOfDeadSalz);
            getoeteteOber.push(numberOfDeadOber);
            getoeteteNieder.push(numberOfDeadNieder);
            getoeteteKaertner.push(numberOfDeadKaertner);
            getoeteteBurg.push(numberOfDeadBurg);
        });

        const labels = berichtsJahreUnique;

        router.get('/', function (req: any, res: any) {
            res.render('statistics/graph', {
                title: 'Graph',
                labels: labels,
                getoeteteWien: getoeteteWien,
                getoeteteVor: getoeteteVor,
                getoeteteTirol: getoeteteTirol,
                getoeteteSteir: getoeteteSteir,
                getoeteteSalz: getoeteteSalz,
                getoeteteOber: getoeteteOber,
                getoeteteNieder: getoeteteNieder,
                getoeteteKaertner: getoeteteKaertner,
                getoeteteBurg: getoeteteBurg
            })
        })
    });

module.exports = router