"use strict";
module.exports = class User {
    constructor(firstName, lastName, age, position, imagePath) {
        this._firstName = firstName;
        this._lastName = lastName;
        this._age = age;
        this._position = position;
        this._imagePath = imagePath;
    }
    get firstName() {
        return this._firstName;
    }
    set firstName(value) {
        this._firstName = value;
    }
    get lastName() {
        return this._lastName;
    }
    set lastName(value) {
        this._lastName = value;
    }
    get age() {
        return this._age;
    }
    set age(value) {
        this._age = value;
    }
    get position() {
        return this._position;
    }
    set position(value) {
        this._position = value;
    }
    get imagePath() {
        return this._imagePath;
    }
    set imagePath(value) {
        this._imagePath = value;
    }
};
