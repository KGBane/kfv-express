`cd kfv-express`  
`npm install`

On MacOS or Linux, run the app with this command:  
`DEBUG=kfv-express:* npm start`

On Windows Command Prompt, use this command:  
`set DEBUG=kfv-express:* & npm start`

On Windows PowerShell, use this command:  
`$env:DEBUG='kfv-express:*'; npm start`

Load http://localhost:3000/ in your browser to access the app.