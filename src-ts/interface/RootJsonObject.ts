import {Verkehrstote} from "./Verkehrstote";

export interface RootJsonObject {
    verkehrstote: Verkehrstote[];
}