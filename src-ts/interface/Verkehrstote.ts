export interface Verkehrstote {
    readonly Berichtsjahr: number;
    Monat_ID: number;
    Stunde_ID: number;
    Wochentag_ID: number;
    readonly Bundesland_ID: number;
    Gebiet_ID: number;
    Verkehrsart_ID: number;
    AlterGr_ID: number;
    Geschlecht_ID: number;
    Ursache_ID: number;
    readonly Getötete: number;
}