module.exports = class User
{
    private _firstName: string
    private _lastName: string
    private _age: number
    private _position: string
    private _imagePath: string

    constructor(firstName: string, lastName: string, age: number, position: string, imagePath: string) {
        this._firstName = firstName
        this._lastName = lastName
        this._age = age
        this._position = position
        this._imagePath = imagePath
    }

    get firstName(): string {
        return this._firstName;
    }

    set firstName(value: string) {
        this._firstName = value;
    }

    get lastName(): string {
        return this._lastName;
    }

    set lastName(value: string) {
        this._lastName = value;
    }

    get age(): number {
        return this._age;
    }

    set age(value: number) {
        this._age = value;
    }

    get position(): string {
        return this._position;
    }

    set position(value: string) {
        this._position = value;
    }

    get imagePath(): string {
        return this._imagePath;
    }

    set imagePath(value: string) {
        this._imagePath = value;
    }
}