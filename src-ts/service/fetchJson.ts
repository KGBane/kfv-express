// type FetchParams = Parameters<typeof window.fetch>
//
// export const fetchJson = <T>(...params: FetchParams): Promise<T> | undefined => {
//     return window.fetch(...params).then((resp) => resp.json() as Promise<T>)
// }