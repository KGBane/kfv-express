"use strict";
exports.__esModule = true;
exports.fetchJson = void 0;
var fetchJson = function () {
    var params = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        params[_i] = arguments[_i];
    }
    return window.fetch.apply(window, params).then(function (resp) { return resp.json(); });
};
exports.fetchJson = fetchJson;
